---
layout:     post
title: "用C调用windows sdk写第一个程序"
#subtitle: 
date: 2018-07-15 12:00
catalog: true
tags:
- C
- C++
- windows sdk
---
### 前言
有些小伙伴可能学了C/C++很长时间还在命令行下晃悠，而在windows上如果想要用C/C++写窗口程序，那最原生态的方法，就是调用
> windows sdk
或者也叫
> win32
了。
但，windows sdk这玩意儿要写出一个啥也没有的窗口，都要几十行代码，这简直看到都头大，而且全是一堆没接触过的变量类型。
所以综上所述，我们先来弄个信息框(也是对话框)看看。
### 实力
先看下面的代码，这可能是你用windows sdk写的最少的代码了：``` c
#include<windows.h>

    int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
PSTR szCmdLine, int iCmdShow)
{
	MessageBox(NULL, TEXT("你好"), TEXT("第一个程序"), 0);
	return 0;
}```
### 运行效果
上面的代码运行后会显示一个信息框，标题是第一个程序，内容是你好，然后有一个确定按钮，点了后程序退出。
虽然功能很简单，但当中开头就遇到了我们没接触过的，下面就开始解释。
### windows.h头文件
先看第一行
> `#include<windows.h>`
这个头文件中包含了写windows程序需要的各种东西，并且他也包含着其他一些基础的，很重要的头文件，如
> windef.h 基本型态定义。
> winnt.h 支持unicode的型态定义。
> winbase.h kernel函数。
> winuser.h 使用者接口函数。
> wingdi.h 图形设备接口函数。
这些表头文件定义了windows的所有数据型态、函数呼叫、数据结构和常量标识符，它们是windows文件中的一个重要部分。
### WinMain函数
接下来，我们定义了一个函数
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	PSTR szCmdLine, int iCmdShow)
这个函数的名字是
<WinMain
其实从名字上就能看出了，它就是windows下的main函数。
它的作用跟main函数一样，当程序运行的时候，就从这个函数开始，也就是入口点函数了。
但，可以看到这个函数前面有一个
<WINAPI
这个我们暂时先不用管，它是声明了一种调用方式。
而剩下的参数，我们也先不管，等写真正的窗口的时候会说。
小贴士
WinMain的声明是在
> winbase.h
头文件里的。
### 信息框函数
下面这个函数
	MessageBox(NULL, TEXT("你好"), TEXT("第一个程序"), 0);
很顾名思义，就是显示信息框的函数。
我们先来看一下它的声明
int WINAPI MessageBox(HWND hWnd, LPCTSTR lpText,LPCTSTR lpCaption,UINT uType);
这些参数的类型一如既往的没见过，但我们暂时也不必深究，知道每个是干什么的就可以了。
第一个参数是窗口的句柄，暂时也不管。
第二个是信息框的内容，虽然他的类型是
> LPCTSTR
但我们可以直接吧指向char数组的指针传给它，或者是字符串字面值，至于为什么类型是LPCTSTR，以后会说。
但有一点要注意，我们之前传给它的参数是这样的
> TEXT("你好")
这个TEXT是一个宏，至于它的作用以后会说，现在只要知道，吧字符串字面值给封装在TEXT里是一个好习惯。
对了，另外要提示一点，这个参数是不能用string的，如果要用string，那只能这样
string.c_str()
或者
string.data()
第三个参数是信息框的标题，类型同上。
第四个是表明信息框的样式，比如有哪些按钮，哪些图标。
这个参数我们可以直接拿数字表示，比如按钮的就有
---
0 确定按钮
1 确定跟取消
3 是否取消
4 是否
等
---
图标的画有
---
64 一个感叹号
16 错误图标
32 询问图标
48 警告图标
等
---
要把图标跟按钮同时显示的画，可以吧他们的数字给相加，比如65，这是一个感叹号图标64，加上一个按钮确定1，最终得到65
这是用数字来表示这些，我们也可以用
> winuser.h
中定义的一组以前缀
> MB
开始的常量的组合。
PS 下面显示的是常量名跟相对应的值
按钮的有
---
#define MB_OK 0x00000000L
#define MB_OKCANCEL 0x00000001L
#define MB_ABORTRETRYIGNORE 0x00000002L
#define MB_YESNOCANCEL 0x00000003L
#define MB_YESNO 0x00000004L
#define MB_RETRYCANCEL 0x00000005L
#define MB_DEFBUTTON1 0x00000000L
#define MB_DEFBUTTON2 0x00000100L
#define MB_DEFBUTTON3 0x00000200L
#define MB_DEFBUTTON4 0x00000300L
---
图标的有
---
#define MB_ICONHAND 0x00000010L
#define MB_ICONQUESTION 0x00000020L
#define MB_ICONEXCLAMATION  0x00000030L
#define MB_ICONASTERISK 0x00000040L
---
另外，图标还有一些代替的值
---
#define MB_ICONWARNINGMB_ICONEXCLAMATION
#define MB_ICONERROR  MB_ICONHAND
#define MB_ICONINFORMATIONMB_ICONASTERISK
#define MB_ICONSTOP   MB_ICONHAND
---
这些图标跟按钮要加一块的画可以用
|
从而把他们给组合在一块
另外这个MessageBox的返回值是根据你按的不同按钮，返回不同的值，比如上面的例子，就一个确定按钮，我们按了过后返回的是1，准确的说，是
> IDOK
类似的还有
IDYES
IDNO
IDCANCEL
IDABORT
IDRETRY
IDIGNORE
---
上面说的按钮跟图标之类的，可以自己尝试看看。
### 后续
虽然说我们只弄了一个信息框吧，都不能称得上是个真正的windows窗口程序，但是，这将是你迈步windows sdk世界的第一步！