function toFocus(selector, op) {
var els = $(selector);
var len = els.length;
var fs = $(document.activeElement);
var index = fs.length > 0 ?els.index(fs) : -1;
if(op == '+') {
index++;
if(index >= len) {
index = 0;
}
} else if(op == '-') {
index--;
if(index < 0) {
index = len - 1;
}
}
els[index].focus();
}

function addAcc(){
if(!!window.ActiveXObject || "ActiveXObject" in window) {
// alt + x and alt + z and alt + c
$("[accesskeyc='c']").attr('accesskey', 'c');
$('.accesskeyx').attr('accesskey', 'x');
$('.accesskeyz').attr('accesskey', 'z');
} else {
$(document).on('keydown', function(e) {
if(e.altKey && e.shiftKey && e.keyCode == 88) {
toFocus('.accesskeyx', '-');
return false;
}
if(e.altKey && e.keyCode == 88) {
toFocus('.accesskeyx', '+');
return false;
}

if(e.altKey && e.shiftKey && e.keyCode == 90) {
toFocus('.accesskeyz', '-');
return false;
}
if(e.altKey && e.keyCode == 90) {
toFocus('.accesskeyz', '+');
return false;
}

if(e.altKey && e.shiftKey && e.keyCode == 67) {
toFocus("[accesskeyc='c']", '-');
return false;
}
if(e.altKey && e.keyCode == 67) {
toFocus("[accesskeyc='c']", '+');
return false;
}

});
}
}